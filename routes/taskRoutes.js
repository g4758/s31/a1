const express = require('express');
const router = express.Router();

const {
	createTask,
	getAllTasks,
	deleteTask,
	updateTask,
	getSpecificTask,
	findOneTask,
	completeTask

} = require('./../controllers/taskControllers')

//CREATE A TASK
router.post('/', async (req, res) => {
	// console.log(req.body)	{"name": "watching"}	//object

	try{
		await createTask(req.body).then(result => res.send(result))

	} catch(err){
		res.status(400).json(err.message)

	} 
})
 

//GET ALL TASKS
router.get('/', async (req, res) => {
	const qId = req.query.id
	const qStatus = req.query.status

	console.log(qId)
	console.log(qStatus)

	await getAllTasks(qId, qStatus).then(result => res.send(result))

	// try{
	// 	await getAllTasks().then(result => res.send(result))

	// } catch(err){
	// 	res.status(500).json(err)
	// }
})

//DELETE A TASK

router.delete('/:id/delete', async (req, res) => {
	// console.log(typeof req.params)	//object
	//console.log(typeof req.params.id)	//string

	try{
		await deleteTask(req.params.id).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})



//UPDATE A TASK
router.put('/', async (req, res) => {
	// console.log(req.params.taskId)
	const id = req.params.taskId
	// console.log(req.body)

	// await updateTask(id, req.body).then(response => res.send(response))

	await updateTask(req.body.name, req.body).then(response => res.send(response))
})

//GET A SPECIFIC TASK
router.get('/:id', async (req, res) => {
	//console.log(req.params)	//{ id: '6226d2aa96fba6a59fcbc4e1' } //object

	// await getSpecificTask(req.params.id).then(result => res.send(result))


	try{
		await getSpecificTask(req.params.id).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


//UPDATE A SPECIFIC TASK TO CHANGE STATUS TO COMPLETE
router.put('/:id/complete', async (req, res) => {

	try{
		await completeTask(req.params.id).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})

module.exports = router;
